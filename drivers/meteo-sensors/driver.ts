import Homey from 'homey';
import httpRequest from '../../utilites/httpRequest';
import { Device, DeviceFullInfo, meteoPairingInfo } from '../../utilites/interfaces';

class MeteoSensorsDriver extends Homey.Driver {

    async onInit() {
        this.log('Meteo Sensors Driver has been initialized');
    }

    async onPairListDevices() {

        const meteoSensors: meteoPairingInfo[] = [];
        try {
            const devicesArray: Device[] = this.homey.env.LOOKinDevices;

            for (let device of devicesArray) {
                const IP = device.IP;
                let deviceInfo: DeviceFullInfo = JSON.parse(await httpRequest(IP, '/device'));
                let version = parseInt(deviceInfo.MRDC.slice(0, 2));
                if (version >= 2) {
                    meteoSensors.push({
                        name: `${device.ID} meteo sensors`,
                        data: {
                            id: deviceInfo.ID
                        },
                        store: {
                            ID: device.ID
                        }
                    });
                }

            }

            return meteoSensors;

        } catch {
            return [];
        }
    }

}

module.exports = MeteoSensorsDriver;
