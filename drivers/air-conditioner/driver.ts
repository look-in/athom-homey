import Homey from 'homey';
import { Device, pairingInfo, RemoteController } from '../../utilites/interfaces';

class AirConditionerDriver extends Homey.Driver {

    async onInit() {
        this.log('Air Conditioner Driver has been initialized');
    }

    async onPairListDevices() {

        const pairingList: pairingInfo[] = [];
        try {
            const devicesArray: Device[] = this.homey.env.LOOKinDevices;
            const airConditioners: RemoteController[] = [];

            devicesArray.forEach(device => {
                const savedRemotes: RemoteController[] | undefined = device.savedRC;
                if (savedRemotes && savedRemotes.length > 0) {
                    const ACRemotes = savedRemotes.filter(remote => remote.Type === 'EF');
                    airConditioners.push(...ACRemotes);
                }
            });

            airConditioners.forEach(item => {
               pairingList.push({
                   name: `${item.deviceInfo.Name}`,
                   data: {
                       id: `${item.deviceInfo.Name}${item.UUID}`
                   },
                   store: {
                       IP: item.IP,
                       ID: item.ID,
                       UUID: item.UUID,
                       status: item.deviceInfo.Status,
                       codeset: item.deviceInfo.Extra
                   }
               })
            });

            return pairingList;

        } catch {
            return [];
        }
    }

}

module.exports = AirConditionerDriver;
