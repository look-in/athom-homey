import Homey from 'homey';
import { Device, pairingInfo, RemoteController } from '../../utilites/interfaces';

class SwitchDriver extends Homey.Driver {

    async onInit() {
        this.log('Switch Driver has been initialized');
    }

    async onPairListDevices() {

        const pairingList: pairingInfo[] = [];
        try {
            const devicesArray: Device[] = this.homey.env.LOOKinDevices;
            const switches: RemoteController[] = [];

            devicesArray.forEach(device => {
                const savedRemotes: RemoteController[] | undefined = device.savedRC;
                if (savedRemotes && savedRemotes.length > 0) {
                    const ACRemotes = savedRemotes.filter(remote => remote.Type === '06');
                    switches.push(...ACRemotes);
                }
            });

            switches.forEach(item => {
                pairingList.push({
                    name: `${item.deviceInfo.Name}`,
                    data: {
                        id: `${item.deviceInfo.Name}${item.UUID}`
                    },
                    store: {
                        IP: item.IP,
                        ID: item.ID,
                        UUID: item.UUID,
                        functions: item.deviceInfo.Functions,
                        status: item.deviceInfo.Status
                    }
                })
            });

            return pairingList;

        } catch {
            return [];
        }
    }

}

module.exports = SwitchDriver;
