import Homey from 'homey';
import udpServer from './utilites/UDPserver';
import { emitter } from './utilites/UDPserver';
import { Device } from './utilites/interfaces';

export class LOOKinApp extends Homey.App {

    /**
     * onInit is called when the app is initialized.
     */
    async onInit() {
        try {
            this.homey.env.LOOKinDevices = [];

            await udpServer();
            emitter.on('new_device', (deviceData: Device) => {
                    const existingDevices: Device[] = this.homey.env.LOOKinDevices;
                    if (!existingDevices.find(existingDevice => existingDevice.ID === deviceData.ID)) {
                        this.homey.env.LOOKinDevices.push(deviceData);
                    }
                }
            );

        } catch (err: any) {
            console.log('UDP server error:', err.stack);
        }
        this.log('The App has been initialized');

    }

}


module.exports = LOOKinApp;
